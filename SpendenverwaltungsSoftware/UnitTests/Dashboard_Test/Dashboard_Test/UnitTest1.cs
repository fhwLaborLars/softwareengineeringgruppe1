﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpendenverwaltungsSoftware;
using System.IO;


namespace Dashboard_Test
{


    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TEST_DashboardDaten()
        {

            //Erzeuge Testdaten für Anzeige:

            string MaximaleSpender = "5000";
            string Spendengeldsumme = "10000";
            string letzteSpende = "25.05.2019";


            //TEST
            Dashboard_Seite TestKlasse = new Dashboard_Seite();
            bool KorrekteAnzeige = TestKlasse.DashboardWerteBerechnenTEST(MaximaleSpender,Spendengeldsumme,letzteSpende);
            Assert.AreEqual(KorrekteAnzeige, true);



        }
    }
}
