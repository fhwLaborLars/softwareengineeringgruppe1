﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Installationsroutine
{
    /// <summary>
    /// Interaktionslogik für Willkommen_Seite.xaml
    /// </summary>
    public partial class Willkommen_Seite : Page
    {


        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);
            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Abbrechen";
                    btn_next.Content = "Weiter";
                    btn_back.Content = "Zurück";
                    lbl_intro.Content = "Dieser Assistent wird Sie durch die Installation der SpendenSoftware begleiten.";
                    lbl_licensetip.Content = "Ich habe die Lizenvereinbarung gelesen";
                    lbl_Title.Content = "Willkommen beim Installations Assistenten";
                    lbl_TitleLicense.Content = "Lizenzvereinbarung";                                  
                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Cancel";
                    btn_next.Content = "Next";
                    btn_back.Content = "Back";
                    lbl_intro.Content = "This assistant will guide you through the installation process";
                    lbl_licensetip.Content = "I have read the license agreement";
                    lbl_Title.Content = "Welcome to the installation Wizard";
                    lbl_TitleLicense.Content = "License agreement";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Abortar";
                    btn_next.Content = "Siguiente";
                    btn_back.Content = "Atrás";
                    lbl_intro.Content = "Este asistente te guiará a través del proceso de instalación.";
                    lbl_licensetip.Content = "He leído el acuerdo de licencia";
                    lbl_Title.Content = "Bienvenido al asistente de instalación.";
                    lbl_TitleLicense.Content = "contrato de licencia";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Avorter";
                    btn_next.Content = "Suivant";
                    btn_back.Content = "Retour";
                    lbl_intro.Content = "cet assistant vous guidera à travers le processus d'installation";
                    lbl_licensetip.Content = "j'ai lu le contrat de licence";
                    lbl_Title.Content = "Bienvenue dans l'assistant d'installation";
                    lbl_TitleLicense.Content = "contrat de licence";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    btn_cancel.Content = "отменить";
                    btn_next.Content = "следующий";
                    btn_back.Content = "назад";
                    lbl_intro.Content = "этот помощник проведет вас через процесс установки";
                    lbl_licensetip.Content = "я прочитал лицензионное соглашение";
                    lbl_Title.Content = "Добро пожаловать в помощник по установке";
                    lbl_TitleLicense.Content = "лицензионное соглашение";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }


        //InitSeite
        public Willkommen_Seite()
        {
            InitializeComponent();
            SpracheEinstellen();
            this.UpdateLayout();
            //Button deaktivieren bis Lizent Checkbox angehakt wurde.
            btn_next.IsEnabled = false;

        }

        //Navigation
        private void Btn_next_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Anforderungen_Seite.xaml", UriKind.Relative));
        }

        //Bennden
        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void TextBlock_TextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void Btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Sprache_Seite.xaml", UriKind.Relative));

        }

        private void Cb_license_Checked(object sender, RoutedEventArgs e)
        {
            //Aktiviere next button 
            btn_next.IsEnabled = true;
        }
    }
}
