﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Installationsroutine
{
    /// <summary>
    /// Interaktionslogik für Pfad_Seite.xaml
    /// </summary>
    /// 



    public partial class Pfad_Seite : Page
    {


        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);
            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    lbl_CheckboxDatapath.Content = "Pfad zu einer existierenden Datenbank";
                    lbl_DbPath.Content = "Wählen Sie den Pfad zur bereits existierenden Datenbank aus.";
                    lbl_ProgramPath.Content = "Wählen Sie den gewünschten Ort in dem das Programm installiert wird.";
                    lbl_Title.Content = "Installationspfad";
                    btn_cancel.Content = "Abbrechen";
                    btn_next.Content = "Weiter";
                    btn_back.Content = "Zurück";
                    btn_DbPath.Content = "Durchsuchen";
                    btn_ProgDir.Content = "Durchsuchen";
                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    lbl_CheckboxDatapath.Content = "Path to existing Database";
                    lbl_DbPath.Content = "Choose the existing Database directory";
                    lbl_ProgramPath.Content = "Choose the directory for software install";
                    lbl_Title.Content = "Choose Installation";
                    btn_cancel.Content = "Cancel";
                    btn_next.Content = "Next";
                    btn_back.Content = "Back";
                    btn_DbPath.Content = "Search";
                    btn_ProgDir.Content = "Search";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    lbl_CheckboxDatapath.Content = "Ruta a la base de datos existente";
                    lbl_DbPath.Content = "Seleccione su ruta a la base de datos existente";
                    lbl_ProgramPath.Content = "Seleccione su directorio de instalación";
                    lbl_Title.Content = "Instalar directorio";
                    btn_cancel.Content = "Abortar";
                    btn_next.Content = "Siguiente";
                    btn_back.Content = "Atrás";
                    btn_DbPath.Content = "Buscar";
                    btn_ProgDir.Content = "Buscar";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    lbl_CheckboxDatapath.Content = "Chemin vers la base de données existante";
                    lbl_DbPath.Content = "Choisissez le répertoire de base de données existant";
                    lbl_ProgramPath.Content = "Choisissez le répertoire d'installation du logiciel";
                    lbl_Title.Content = "Choisissez l'installation";
                    btn_cancel.Content = "Avorter";
                    btn_next.Content = "Suivant";
                    btn_back.Content = "Retour";
                    btn_DbPath.Content = "Chercher";
                    btn_ProgDir.Content = "Chercher";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    lbl_CheckboxDatapath.Content = "Путь к существующей базе данных";
                    lbl_DbPath.Content = "Выберите существующий каталог базы данных";
                    lbl_ProgramPath.Content = "Выберите каталог для установки программного обеспечения";
                    lbl_Title.Content = "установить путь";
                    btn_cancel.Content = "отменить";
                    btn_next.Content = "следующий";
                    btn_back.Content = "назад";
                    btn_DbPath.Content = "поиск";
                    btn_ProgDir.Content = "поиск";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }




        public Pfad_Seite()
        {


            InitializeComponent();
            //Setze Standardpfad zu
            // C:\Program Files\HeLaTi
            SpracheEinstellen();

            try
            {
                tb_programpath.Text = Environment.GetEnvironmentVariable("ProgramFiles(x86)") + "\\HeLaTi" + "\\SpendenverwaltungsSoftware";
                tb_Database.Text = Environment.GetEnvironmentVariable("ProgramFiles(x86)") + "\\HeLaTi" + "\\SpendenverwaltungsSoftware";
                //Setzte zu Beginn Datenbank Pfad als nicht Sichtbar
                lbl_DbPath.Visibility = Visibility.Hidden;
                btn_DbPath.Visibility = Visibility.Hidden;
                tb_Database.Visibility = Visibility.Hidden;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }



        private void Btn_DbPath_Click(object sender, RoutedEventArgs e)
        {


            //Öffne Auswahlfenster zur Auswahl der Datei
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "db Dateien (*.db)|*.db";
                ofd.ValidateNames = true;
                ofd.Multiselect = false;

                Nullable<bool> result = ofd.ShowDialog();
                if (result == true)
                {
                    tb_Database.Text = ofd.InitialDirectory + ofd.FileName; // Füge Dateinamen in Textfeld ein
                }
                //Füge den Datenbank Pfad der Settings.ini hinzu
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\Settings.ini", "\r\n" + "Datenbankpfad=" + tb_Database.Text);
            }
            //Gebe Fehler mit seiner zugehoerigen Meldung als PopUp Fenster aus
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
                System.Environment.Exit(1);
            }



        }


        private void Btn_ProgDir_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                // Erstelle neuen Speichern unter Dialog
                var dialog = new Microsoft.Win32.SaveFileDialog();
                dialog.InitialDirectory = tb_Database.Text; // Aktuellen Ordner als Init Ordner nutzen
                dialog.Title = "Select a Directory"; // Anstatt eine Datei "Select a Dir"
                dialog.Filter = "Directory|*.this.directory"; // Verhidnert die Ansicht von Dateien
                dialog.FileName = "select"; // Ordner Pfad"
                if (dialog.ShowDialog() == true)
                {
                    string path = dialog.FileName;
                    // Entferne "Fake" Filename von Pfad
                    path = path.Replace("\\select.this.directory", "");
                    path = path.Replace(".this.directory", "");
                    // Wenn Filename geändert wurde erstelle neuen Ordner
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    // Finaler Installationspfad:
                    tb_programpath.Text = path;
                    //Erstelle in der Settings.ini den Programmpfad wohin das Programm installiert wird.
                    //Dieser Wert wird auf der nächsten Seite benötigt
                    File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\Settings.ini", "\r\n" + "Programmpfad=" + tb_programpath.Text);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
                System.Environment.Exit(1);
            }

        }





        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void Btn_next_Click(object sender, RoutedEventArgs e)
        {

            //Wenn Datenpfad leer bleibt setze den Standard Pfad im Root Verzeichnis
            if(tb_Database.Text == "")
            {
                File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\Settings.ini", "\r\n" + "Datenbankpfad=" + tb_programpath.Text + "\\SpenderVerzeichnis.db");
            }
            else
            {
                this.NavigationService.Navigate(new Uri("Installation_Seite.xaml", UriKind.Relative));

            }




        }

        private void Btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Anforderungen_Seite.xaml", UriKind.Relative));
        }

        private void Cb_DBPath_Click(object sender, RoutedEventArgs e)
        {


                if (cb_DBPath.IsChecked == true)
                {
                    lbl_DbPath.Visibility = Visibility.Visible;
                    btn_DbPath.Visibility = Visibility.Visible;
                    tb_Database.Visibility = Visibility.Visible;
                }
                else
                {
                    lbl_DbPath.Visibility = Visibility.Hidden;
                    btn_DbPath.Visibility = Visibility.Hidden;
                    tb_Database.Visibility = Visibility.Hidden;
                }
            }      
    }
}
