﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Installationsroutine
{
    /// <summary>
    /// Interaktionslogik für Sprache_Seite.xaml
    /// </summary>
    public partial class Sprache_Seite : Page
    {







        //INIT Seite
        public Sprache_Seite()
        {
            InitializeComponent();
        }


        //Button Aktion
        private void Btn_next_Click(object sender, RoutedEventArgs e)
        {

            //Initialisiere Variablen
            string Sprachauswahl;
            Sprachauswahl = "NoItem";

            //Weise den Flaggen aus dem Container mit ID entsprechende Strings zu
            //_________________________________________________________
            switch (lb_Sprachauswahl.SelectedIndex)
            {
                case -1:
                    Console.WriteLine("Kein Item ");
                    Sprachauswahl = "noitem";
                    break;
                case 0:
                    Console.WriteLine("Deutsch");
                    Sprachauswahl = "Deutsch";             
                    break;
                case 1:
                    Console.WriteLine("Englisch");
                    Sprachauswahl = "Englisch";
                    break;
                case 2:
                    Console.WriteLine("Spanisch ");
                    Sprachauswahl = "Spanisch";
                    break;
                case 3:
                    Console.WriteLine("Französisch");
                    Sprachauswahl = "Französisch";
                    break;
                case 4:
                    Console.WriteLine("Russisch");
                    Sprachauswahl = "Russisch";
                    break;
            }


            //Schreibe die Ausgewählte Sprache in die Settings.ini
            //_________________________________________________________
            try
            {
                File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "\\Settings.ini", "Sprache=" + Sprachauswahl);
                //Öffne die Nächste Seite
                this.NavigationService.Navigate(new Uri("Willkommen_Seite.xaml", UriKind.Relative));
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
                System.Environment.Exit(1);
            }
        }

        //Programm Beenden
        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);

        }
    }
}
