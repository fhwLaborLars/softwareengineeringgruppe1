﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IWshRuntimeLibrary;
using System.IO;
namespace Installationsroutine
{
    /// <summary>
    /// Interaktionslogik für Fertiggestellt_Seite.xaml
    /// </summary>
    public partial class Fertiggestellt_Seite : Page
    {

        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);
            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    btn_ready.Content = "Fertigstellen";
                    lbl_fertig.Content = "Fertig";
                    lbl_InstallMsg.Content = "Die Software wurde erfolgreich installiert.";
                    lbl_shortcut.Content = "Möchten Sie eine Verknüpfung auf dem Desktop erstellen ? ";
                    lbl_Title.Content = "Installation";
                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    btn_ready.Content = "Finish";
                    lbl_fertig.Content = "Complete";
                    lbl_InstallMsg.Content = "Software installation was succesfull.";
                    lbl_shortcut.Content = "Would you like to create a Desktop Shortcut?";
                    lbl_Title.Content = "Installation";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    btn_ready.Content = "Completo";
                    lbl_fertig.Content = "Terminar";
                    lbl_InstallMsg.Content = "La instalación del software fue exitosa";
                    lbl_shortcut.Content = "Te gustaría crear un acceso directo de escritorio?";
                    lbl_Title.Content = "Instalación";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    btn_ready.Content = "Complet";
                    lbl_fertig.Content = "Terminer";
                    lbl_InstallMsg.Content = "L'installation du logiciel a réussi";
                    lbl_shortcut.Content = "Voulez-vous créer un raccourci sur le bureau?";
                    lbl_Title.Content = "Installation";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    btn_ready.Content = "полный";
                    lbl_fertig.Content = "отделка";
                    lbl_InstallMsg.Content = "Установка программного обеспечения прошла успешно";
                    lbl_shortcut.Content = "Хотите создать ярлык на рабочем столе?";
                    lbl_Title.Content = "монтаж";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }
        string ErmitteleProgrammInstallationspfad()
        {

            //Suche Installationspfad aus Settings.ini heraus 
            //Erstelle dafür ein Array und Suche nach der Zeile in dem der Programmpfad steht. (Falls die DB zuerst gewählt wurde, steht diese 
            //in der 2. Zeile und der Pfad in der 3.)
            var StringArray = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\" + "Settings.ini");
            //Variable Programminstallationspfad
            string ProgrammInstallalationsPfad = "default";
            //Gebe Anzahl der Elemente im String Array zurück
            int StringArrayCount = StringArray.Count();
            Console.WriteLine("Maximale Anzahl der StringElemente: " + StringArrayCount);

            for (int i = 0; i < StringArrayCount; i++)
            {

                if (StringArray[i].Contains("Programmpfad") == true)
                {
                    ProgrammInstallalationsPfad = StringArray[i];
                    //Schneide das Programmpfad von String ab
                    string[] Split = ProgrammInstallalationsPfad.Split('=');
                    ProgrammInstallalationsPfad = Split[1];
                    Console.WriteLine("Programmpfad in Zeile: " + i + " gefunden.");

                }
                else
                {
                    Console.WriteLine("Programmpfad nicht in Zeile: " + i + " gefunden.");
                }



            }
            Console.WriteLine("Zurückgegebener Programmpfad: " + ProgrammInstallalationsPfad);
            return ProgrammInstallalationsPfad;



        }




        public Fertiggestellt_Seite()
        {
            InitializeComponent();
            SpracheEinstellen();
        }

        private void Btn_ready_Click(object sender, RoutedEventArgs e)
        {

            if(cb_Shortcut.IsChecked == true)
            {

                try
                {
                    //Erstelle Verknüpfung
                    WshShell shell = new WshShell();
                    IWshShortcut link = (IWshShortcut)shell.CreateShortcut(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Spendenverwaltung.lnk");
                    link.IconLocation = ErmitteleProgrammInstallationspfad() + "\\Resources\\favicon.ico";
                    link.TargetPath = ErmitteleProgrammInstallationspfad() + "\\SpendenverwaltungsSoftware.exe";
                    Console.WriteLine(ErmitteleProgrammInstallationspfad() + "\\SpendenverwaltungsSoftware.exe");
                    link.Save();
                    System.Environment.Exit(1);


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                //Verlasse Programm ohne Verknüpfung
                System.Environment.Exit(1);

            }


        }
    }
}
