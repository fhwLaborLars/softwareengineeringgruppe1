﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Compression;
using System.ComponentModel;
using System.Threading;
using System.IO;

namespace Installationsroutine
{
    /// <summary>
    /// Interaktionslogik für Installation_Seite.xaml
    /// </summary>
    public partial class Installation_Seite : Page
    {


        //Funktion zum ermitteln des Installationspfades
        string ErmitteleProgrammInstallationspfad()
        {

            //Suche Installationspfad aus Settings.ini heraus 
            //Erstelle dafür ein Array und Suche nach der Zeile in dem der Programmpfad steht. (Falls die DB zuerst gewählt wurde, steht diese 
            //in der 2. Zeile und der Pfad in der 3.)
            var StringArray = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "\\" + "Settings.ini");
            //Variable Programminstallationspfad
            string ProgrammInstallalationsPfad = "default" ;
            //Gebe Anzahl der Elemente im String Array zurück
            int StringArrayCount = StringArray.Count();
            Console.WriteLine("Maximale Anzahl der StringElemente: " + StringArrayCount);

            for(int i=0; i < StringArrayCount; i++)
            {

                if (StringArray[i].Contains("Programmpfad") == true)
                {
                    ProgrammInstallalationsPfad = StringArray[i];
                    //Schneide das Programmpfad von String ab
                    string[] Split = ProgrammInstallalationsPfad.Split('=');
                    ProgrammInstallalationsPfad = Split[1];
                    Console.WriteLine("Programmpfad in Zeile: " + i + " gefunden.");

                }
                else
                {
                    Console.WriteLine("Programmpfad nicht in Zeile: " + i + " gefunden.");
                }



            }
            Console.WriteLine("Zurückgegebener Programmpfad: " + ProgrammInstallalationsPfad);
            return ProgrammInstallalationsPfad;



        }

        //Funktion um das Programmarchiv zu entpacken
        int EntpackeZIP()
        {
            //Variablen Init
            int DateiCounterMax = 0;
            int DateiCounter = 0;
            //Aktuellen Pfad zur Resource die Installiert werden soll
            string ZipPath = System.IO.Path.GetTempPath() + "HeLaTi" + "Source.zip";
            //Kopiere in Temp Verzeichnis um von dort aus zu installieren (Wegen CD .iso Problem)
            System.IO.File.Copy(Environment.CurrentDirectory + "\\Source.zip", ZipPath, true);
            Console.WriteLine(ZipPath);
            //Aktuellen Programminstallationspfad mit Funktion ermitteln
            string ProgrammInstallalationsPfad = ErmitteleProgrammInstallationspfad();

            //Ermitteln der Elemente im Archiv
            try
            {
                //Handle Zip Archive
                using (ZipArchive archive = ZipFile.OpenRead(ZipPath))
                {

                    //Zähle die Gesamt vorhanden Dateien in der ZIP
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        Console.WriteLine(entry.FullName);
                        DateiCounterMax++;
                    }
                    Console.WriteLine("Maximale Anzahl der Elemente: " + DateiCounterMax);
                    //Lege den Wert des Fortschirttbalkens auf den Maximalen Datei Wert
                    ProgressBarInstallation.Maximum = DateiCounterMax;
                    //Einfache Lösung
                    ZipFile.ExtractToDirectory(ZipPath, ProgrammInstallalationsPfad);

                    //Entpacke Bessere Methode aber noch Probleme mit den Ordnern
                    ///____________________________________________________________________

                    //try
                    //{
                    //    foreach (ZipArchiveEntry entry in archive.Entries)
                    //    {
                    //        entry.ExtractToFile(Pfad_Seite.Globals.FILE_Path + "\\" + entry.Name);
                    //        Console.WriteLine("Entpacke: " + Pfad_Seite.Globals.FILE_Path + entry.Name);
                    //        DateiCounter++;
                    //        ProgressBarInstallation.Value = DateiCounter;
                    //        if (ProgressBarInstallation.Value < DateiCounterMax)
                    //        {
                    //            btn_next.IsEnabled = false;
                    //        }
                    //        else
                    //        {
                    //            btn_next.IsEnabled = true;
                    //        }

                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
                    //    Console.WriteLine("Fehler: " + ex.Message);
                    //    //Einfache Mehtode ohne Richigen Counter gilt hier als Fehlerbehandlung
                    //    //Problem für richtige Methode sind zurzeit Ordner in der ZIP, welche .ExtractToFile nicht verarbeiten kann
                    //    //Lösche vorher entpackte Dateien

                    //}

                    //Fertigstellung
                    MessageBox.Show("Installation Fertiggestellt! Es wurden " + DateiCounterMax + " Dateien Installiert!");
                    System.IO.File.Copy(AppDomain.CurrentDomain.BaseDirectory + "\\" + "Settings.ini", ProgrammInstallalationsPfad + "\\" + "Settings.ini");
                    this.NavigationService.Navigate(new Uri("Fertiggestellt_Seite.xaml", UriKind.Relative));

                    //Einfache Mehtode ohne Richigen Counter gilt hier als Fehlerbehandlung
                    //Problem für richtige Methode sind zurzeit Ordner in der ZIP, welche .ExtractToFile nicht verarbeiten kann
                    //Lösche vorher entpackte Dateien
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }


            return DateiCounter; 
            
        }

        public Installation_Seite()
        {




        }

        private void Btn_next_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);

        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            this.UpdateLayout();
            EntpackeZIP();

        }





    }





}

