﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Management;
using Microsoft.Win32;
using System.IO;

namespace Installationsroutine
{
    /// <summary>
    /// Interaktionslogik für Anforderungen_Seite.xaml
    /// </summary>
    public partial class Anforderungen_Seite : Page
    {


        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);
            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Abbrechen";
                    btn_next.Content = "Weiter";
                    btn_back.Content = "Zurück";
                    lbl_OS.Content = "Betriebssystem";
                    lbl_Hardware.Content = "Hardware";
                    lbl_Software.Content = "Software";
                    lbl_title.Content = "System Anforderungen";
                    lbl_title2.Content = "";
                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Cancel";
                    btn_next.Content = "Next";
                    btn_back.Content = "Back";
                    lbl_OS.Content = "Betriebssystem";
                    lbl_Hardware.Content = "Hardware";
                    lbl_Software.Content = "Software";
                    lbl_title.Content = "System requirements";
                    lbl_title2.Content = "In this step System components will be checked";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Abortar";
                    btn_next.Content = "Siguiente";
                    btn_back.Content = "Atrás";
                    lbl_OS.Content = "Betriebssystem";
                    lbl_Hardware.Content = "Hardware";
                    lbl_Software.Content = "Software";
                    lbl_title.Content = "Requisitos del sistema";
                    lbl_title2.Content = "En este paso se revisarán los componentes del sistema.";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    btn_cancel.Content = "Avorter";
                    btn_next.Content = "Suivant";
                    btn_back.Content = "Retour";
                    lbl_OS.Content = "Betriebssystem";
                    lbl_Hardware.Content = "Hardware";
                    lbl_Software.Content = "Logiciel";
                    lbl_title.Content = "Configuration requise";
                    lbl_title2.Content = "Dans cette étape, les composants du système seront vérifiés";            
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    btn_cancel.Content = "отменить";
                    btn_next.Content = "следующий";
                    btn_back.Content = "назад";
                    lbl_OS.Content = "операционная система";
                    lbl_Hardware.Content = "аппаратные ";
                    lbl_Software.Content = "программного ";
                    lbl_title.Content = "Системные Требования";
                    lbl_title2.Content = "на этом этапе компоненты системы будут проверены";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }


        public Anforderungen_Seite()
        {
            InitializeComponent();
            SpracheEinstellen();
            this.UpdateLayout();
            cb_Betriebssystem.IsEnabled = false;
            cb_Hardware.IsEnabled = false;
            cb_Software.IsEnabled = false;
            getHardwareInfo();
            getOSInfo();
            getSoftwareInfo();
            getHardwareName();




        }

        private void Btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(1);

        }

        private void Btn_next_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Pfad_Seite.xaml", UriKind.Relative));

        }

        private void Btn_back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Willkommen_Seite.xaml", UriKind.Relative));

        }


        public void getHardwareInfo()
        {
            
            var searcher = new ManagementObjectSearcher(
            "select MaxClockSpeed from Win32_Processor");
            foreach (var item in searcher.Get())
            {
                var Taktfrequenz = (uint)item["MaxClockSpeed"];
                Console.WriteLine(Taktfrequenz);
                Convert.ToSingle(Taktfrequenz);

                if (Taktfrequenz < 1000.0)
                {
                    cb_Hardware.IsChecked = false;
                    btn_next.IsEnabled = false;
                    lbl_Hardware_Active.Foreground = Brushes.Red;
                }
                else
                {
                    lbl_Hardware_Active.Foreground = Brushes.Green;
                    cb_Hardware.IsChecked = true;
                }
            }
            
        }
        public void getHardwareName()
        {

            var searcher = new ManagementObjectSearcher(
            "select Name from Win32_Processor");
            foreach (var item in searcher.Get())
            {
                var Prozessorname = (string)item["Name"];
                Console.WriteLine(Prozessorname);
                lbl_Hardware_Active.Content = Prozessorname ;
            }

        }
        public void getOSInfo()
        {
            bool check = false;
            string result = string.Empty;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
            foreach (ManagementObject os in searcher.Get())
            {
                result = os["Caption"].ToString();
                Console.WriteLine(result);
                break;

            }

            if(result.Contains("Windows 10") || result.Contains("Windows 7") || result.Contains("Windows 8"))
            {
                check = true;
                lbl_OS_Active.Foreground = Brushes.Green;
            }
            else
            {
                check = false;
                btn_next.IsEnabled = false;
                lbl_OS_Active.Foreground = Brushes.Red;

            }

            cb_Betriebssystem.IsChecked = check;
            lbl_OS_Active.Content = result;
            


        }
        public void getSoftwareInfo()
        {
            String SoftwareVersion = Environment.Version.ToString();
            Console.WriteLine("Version: {0}", Environment.Version.ToString());
            //Vergleich
            cb_Software.IsChecked = true;
            lbl_Software_Active.Content = ".net Framework Version " + SoftwareVersion;

            //Hinweise zur Version von Microsoft:
            /*
             * Bei den .NET Framework-Versionen 4, 4.5, 4.5.1 und 4.5.2 gibt die Environment.VersionEigenschaft ein Version-Objekt zurück, 
             * dessen Zeichenfolgedarstellung die Form 4.0.30319.xxxxxbesitzt. 
             * Für die .NET Framework 4.6 und höher, er hat das Format 4.0.30319.42000.
 
             */
            //Auswertung zu .net 
            if (SoftwareVersion.Contains("4.0.30319")){

                lbl_Software_Active.Foreground = Brushes.Green;

            }
            else
            {
                lbl_Software_Active.Foreground = Brushes.Red;
                cb_Software.IsChecked = false;


            }


        }


    }
}
