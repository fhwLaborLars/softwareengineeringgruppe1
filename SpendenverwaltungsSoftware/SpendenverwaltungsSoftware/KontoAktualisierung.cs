﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SpendenverwaltungsSoftware
{
    public class KontoAktualisierung
    {
        // Fuer Unit Test
        public bool Connect(string db)
        {
            try
            {
                SQLiteConnection con = new SQLiteConnection("Data Source=" + db + ";Version=3;");
                con.Open();
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        //Erstelle Klasse zum Auslesen von .csv Dateien mithilfe des "OleDb Connectors" und seiner .csv Parsing Funktion
        //der Rückgabewert der Funtkion entspricht einem DataTable Format.
        //______________________________________________________________________________________________________________
        public DataTable ReadCSV(string fileName) // Funktion zur Auslesung einer CSV Datei und Speicherung der Daten in ein DataTable
        {

            DataTable dt = new DataTable("Data");
            try
            {


                using (OleDbConnection cn = new OleDbConnection("Provider=Microsoft.JET.OLEDB.4.0; Data Source=" + Path.GetDirectoryName(fileName) + "; Extended Properties='text;HDR=yes;FMT=Delimited';"))
                {
                    using (OleDbCommand cmd = new OleDbCommand(string.Format("select * from [{0}]", new FileInfo(fileName).Name), cn))
                    {
                        cn.Open();
                        using (OleDbDataAdapter adapter = new OleDbDataAdapter(cmd))
                        {
                            adapter.Fill(dt);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return dt;



        }

        public void WriteCSVtoSQL(string dbName, DataTable dt)
        {
            // Dummy Variablen
            int anzahlImport = 0;
            int anzahlImportSpende = 0;
            int anzahlImportSpender = 0;

            //Datenbankverbindung herstellen
            using (SQLiteConnection con = new SQLiteConnection("Data Source=" + dbName + ";Version=3;"))
            {
                // Verbindung öffnen
                con.Open();

                // Schleife wird für jede Zeile 1 mal durchlaufen
                for (int i = 0; dt.Rows.Count > i; i++)
                {
                    //Versuche Daten zu schreiben/auszulesen
                    try
                    {
                        //Dummy Variable
                        int countSpender = 0;
                        int spenderid = 0;
                        int countSpende = 0;

                        // Neuen Command erstellen
                        SQLiteCommand cmd = new SQLiteCommand(con);

                        // Anzahl vorhandener IBAN-Einträge für aktuelle IBAN ausgeben (entweder 0 oder 1, da UNIQUE)
                        cmd.CommandText = $"SELECT count(*) FROM Spender WHERE IBAN='{dt.Rows[i].ItemArray[5]}';";
                        // Schreibe  Anzahl in Variable
                        countSpender = Convert.ToInt32(cmd.ExecuteScalar());

                        // Wenn IBAN nicht vorhanden ist = Spender nicht vorhanden -> Spender hinzufügen
                        if (countSpender == 0)
                        {
                            // Name, Vorname, IBAN, BIC -> Tabelle "Spender"
                            cmd.CommandText = $"INSERT INTO Spender ({dt.Columns[0].ColumnName},{dt.Columns[1].ColumnName},{dt.Columns[5].ColumnName},{dt.Columns[6].ColumnName}) " +
                                                $"VALUES ('{dt.Rows[i].ItemArray[0]}','{dt.Rows[i].ItemArray[1]}','{dt.Rows[i].ItemArray[5]}','{dt.Rows[i].ItemArray[6]}');";
                            cmd.ExecuteNonQuery();

                            // Anzahl der importierten Spender hochzählen
                            anzahlImportSpender++;
                        }

                        // Gib die aktuelle SpenderID aus
                        cmd.CommandText = $"SELECT SpenderID FROM Spender WHERE IBAN='{dt.Rows[i].ItemArray[5]}';";
                        spenderid = Convert.ToInt32(cmd.ExecuteScalar());

                        // Prüfen, ob Spende mit aktueller SpendeID und aktuellem Datum vorhanden (entweder 0 oder 1, da UNIQUE)
                        cmd.CommandText = $"SELECT count(*) FROM Spende WHERE Spender='{spenderid}' AND Datum='{dt.Rows[i].ItemArray[4]}';";
                        countSpende = Convert.ToInt32(cmd.ExecuteScalar());

                        // Wenn Spende nicht vorhanden, dann Spende eintragen und aktuelle SpenderID hinzufügen
                        if (countSpende == 0)
                        {
                            // Aktuelle SpendeID, Betrag, Waehrung, Datum -> Tabelle "Spende"
                            cmd.CommandText = $"INSERT INTO Spende(Spender,{dt.Columns[2].ColumnName},{dt.Columns[3].ColumnName},{dt.Columns[4].ColumnName}) " +
                                                $"VALUES ('{spenderid}','{dt.Rows[i].ItemArray[2]}','{dt.Rows[i].ItemArray[3]}','{dt.Rows[i].ItemArray[4]}');";
                            cmd.ExecuteNonQuery();

                            // Anzahl importierter Spenden hochzählen
                            anzahlImportSpende++;
                        }

                        // Anzahl der durchgeführten Importvorgänge hochzählen
                        anzahlImport++;

                    }
                    //Fange Exception, falls Fehler auftritt
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }

            }

            // Öffne Fenster und gib anzahlImportSpender und anzahlImportSpende aus
            MessageBox.Show($"{anzahlImportSpender} neue(r) Spender in der Datenbank gespeichert.\n\n" +
                $"{anzahlImportSpende} neue Spende(n) in der Datenbank gespeichert.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

        }
    }
}
