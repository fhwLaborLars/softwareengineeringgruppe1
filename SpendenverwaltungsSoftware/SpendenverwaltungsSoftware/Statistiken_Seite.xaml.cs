﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using System.IO;





namespace SpendenverwaltungsSoftware
{
    /// <summary>
    /// Interaktionslogik für Statistiken_Seite.xaml
    /// </summary>
    public partial class Statistiken_Seite : Page
    {

        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);


            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    tab_Anzahl.Header = "Anzahl";
                    tab_Geschlecht.Header = "Geschlecht";
                    tab_region.Header = "Region";
                    tab_Spendengeld.Header = "Spendenhöhe";
                    tab_waehrung.Header = "Währung";
                    lbl_Title1.Content = "Statistiken zur Anzahl der Spender";
                    lbl_Title2.Content = "Statistiken zum Geschlecht der Spender";
                    lbl_Title3.Content = "Statistiken zur Spendengeldhöhe";
                    lbl_Title4.Content = "Statistiken zur Region der Spender";
                    lbl_Title5.Content = "Statistiken zur Währung der Spender";

                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    tab_Anzahl.Header = "Amount";
                    tab_Geschlecht.Header = "Gender";
                    tab_region.Header = "Region";
                    tab_Spendengeld.Header = "Money amount";
                    tab_waehrung.Header = "Currency";
                    lbl_Title1.Content = "Donator amount statistics";
                    lbl_Title2.Content = "Donator gender statistics ";
                    lbl_Title3.Content = "Donation amount statistics";
                    lbl_Title4.Content = "Donator region statistics";
                    lbl_Title5.Content = "Donation currency statistics";


                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    tab_Anzahl.Header = "Número";
                    tab_Geschlecht.Header = "Sexo";
                    tab_region.Header = "Región";
                    tab_Spendengeld.Header = "Cantidad de dinero";
                    tab_waehrung.Header = "Moneda";
                    lbl_Title1.Content = "Donante cantidad estadística";
                    lbl_Title2.Content = "Donante género estadística ";
                    lbl_Title3.Content = "Donación cantidad estadística";
                    lbl_Title4.Content = "Donante región estadística";
                    lbl_Title5.Content = "Donación moneda estadística";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    tab_Anzahl.Header = "Nombre";
                    tab_Geschlecht.Header = "Sexe";
                    tab_region.Header = "Région";
                    tab_Spendengeld.Header = "Masse monétaire";
                    tab_waehrung.Header = "Monnaie";
                    lbl_Title1.Content = "Donateur nombre statistiques";
                    lbl_Title2.Content = "Donateur sexe statistiques ";
                    lbl_Title3.Content = "Don masse monétaire statistiques";
                    lbl_Title4.Content = "Donateur région statistiques";
                    lbl_Title5.Content = "Don monnaie statistiques";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    tab_Anzahl.Header = "номер";
                    tab_Geschlecht.Header = "закрыто";
                    tab_region.Header = "область";
                    tab_Spendengeld.Header = "денежная масса";
                    tab_waehrung.Header = "валюта";
                    lbl_Title1.Content = "донор количество статистика";
                    lbl_Title2.Content = "донор секс статистика ";
                    lbl_Title3.Content = "пожертвование количество статистика";
                    lbl_Title4.Content = "донор область статистика";
                    lbl_Title5.Content = "пожертвование currency статистика";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }


        public Statistiken_Seite()
        {
            InitializeComponent();
            SpracheEinstellen();
            ChartAnzahlPlot();
            ChartGeschlechtPlot();
            ChartSpendenGeldPlot();
            ChartWaehrungPlot();
            ChartWaehrungPlot();
            GeoMapExample();
        }








        //Funktion Werte für Chart Anzahl
        //__________________________________________________________________________________________
        public SeriesCollection ChartAnzahlData { get; private set; }

        public void ChartAnzahlPlot()
        {

            //Neue Series Collection für Chart Daten erstellen
            ChartAnzahlData = new SeriesCollection();

            //Erstelle mehrere LineSeries (Werte)
            var Testwert11Series = new LineSeries
            {
                Title = "Testwert11",
                Values = new ChartValues<double> { 10, 5, 7, 5, 7, 8 }
            };

            var Testwert12Series = new LineSeries
            {
                Title = "Testwert12",
                Values = new ChartValues<double> { 5, 6, 9, 10, 11, 9 }
            };

            //add our series to our SeriesCollection
            ChartAnzahlData.Add(Testwert11Series);
            ChartAnzahlData.Add(Testwert12Series);

            //that's it, LiveCharts is ready and listening for your data changes.
            DataContext = this;
        }
        //Ende Chart Anzahl_________________________________________________________________________
        //__________________________________________________________________________________________




        //Funktion Werte für Chart Geschlecht
        //__________________________________________________________________________________________
        public SeriesCollection ChartGeschlechtData { get; private set; }

        public void ChartGeschlechtPlot()
        {

            //Neue Series Collection für Chart Daten erstellen
            ChartGeschlechtData = new SeriesCollection();

            //Erstelle mehrere LineSeries (Werte)
            var Testwert21Series = new LineSeries
            {
                Title = "Testwert21",
                Values = new ChartValues<double> { 1000, 200, 30, 90, 150, 500 }
            };

            var Testwert22Series = new LineSeries
            {
                Title = "Testwert22",
                Values = new ChartValues<double> { 5, 6, 9, 10, 11, 9 }
            };

            //add our series to our SeriesCollection
            ChartGeschlechtData.Add(Testwert21Series);
            ChartGeschlechtData.Add(Testwert22Series);

            //that's it, LiveCharts is ready and listening for your data changes.
            DataContext = this;
        }
        //Ende Chart Anzahl_________________________________________________________________________
        //__________________________________________________________________________________________





        //Funktion Werte für Chart Spendenhöhe
        //__________________________________________________________________________________________
        public SeriesCollection ChartSpendenGeldData { get; private set; }

        public void ChartSpendenGeldPlot()
        {

            //Neue Series Collection für Chart Daten erstellen
            ChartSpendenGeldData = new SeriesCollection();

            //Erstelle mehrere LineSeries (Werte)
            var Testwert31Series = new LineSeries
            {
                Title = "Testwert31",
                Values = new ChartValues<double> { 1000, 200, 30, 90, 150, 500 }
            };

            var Testwert32Series = new LineSeries
            {
                Title = "Testwert32",
                Values = new ChartValues<double> { 5, 6, 9, 10, 11, 9 }
            };

            //add our series to our SeriesCollection
            ChartSpendenGeldData.Add(Testwert31Series);
            ChartSpendenGeldData.Add(Testwert32Series);

            //that's it, LiveCharts is ready and listening for your data changes.
            DataContext = this;
        }
        //Ende Chart Anzahl_________________________________________________________________________
        //__________________________________________________________________________________________




        //Funktion Werte für Chart Waehrung
        //__________________________________________________________________________________________
        public SeriesCollection ChartWaehrungData { get; private set; }

        public void ChartWaehrungPlot()
        {

            PieChart ChartWaehrungData = new PieChart();


            PieSeries pieSeries = new PieSeries();
            pieSeries.Values = new ChartValues<int> { 5 };
            pieSeries.Title = "A";
            pieSeries.DataLabels = true;

            PieSeries pieSeries2 = new PieSeries();
            pieSeries2.Values = new ChartValues<int> { 3 };
            pieSeries2.Title = "B";
            pieSeries2.DataLabels = true;

            PieSeries pieSeries3 = new PieSeries();
            pieSeries3.Values = new ChartValues<int> { 6 };
            pieSeries3.Title = "C";
            pieSeries3.DataLabels = true;

            ChartWaehrungData.Series.Add(pieSeries);
            ChartWaehrungData.Series.Add(pieSeries2);
            ChartWaehrungData.Series.Add(pieSeries3);

        }





        public void GeoMapExample()
        {
            string GeoMapResourcePath = AppDomain.CurrentDomain.BaseDirectory + "//Resources//Maps//World.xml";
            var r = new Random();

            var values = new Dictionary<string, double>();

            values["MX"] = r.Next(0, 100);
            values["CA"] = r.Next(0, 100);
            values["US"] = r.Next(0, 100);
            values["IN"] = r.Next(0, 100);
            values["CN"] = r.Next(0, 100);
            values["JP"] = r.Next(0, 100);
            values["BR"] = r.Next(0, 100);
            values["DE"] = r.Next(0, 500);
            values["FR"] = r.Next(0, 100);
            values["GB"] = r.Next(0, 100);

            var lang = new Dictionary<string, string>();
            lang["DE"] = "German"; // change the language if necessary
            GeoMapRegion.HeatMap = values;
            GeoMapRegion.LanguagePack = lang;
            GeoMapRegion.Source = GeoMapResourcePath;
            GeoMapRegion.EnableZoomingAndPanning = true;
            GeoMapRegion.MaxHeight = 400;
            GeoMapRegion.MaxWidth = 750;

        }





    }






















}
