﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Data.SQLite;
using Microsoft.Win32;
using Path = System.IO.Path;





namespace SpendenverwaltungsSoftware
{
    /// <summary>
    /// Interaktionslogik für SpenderManuell_seite.xaml
    /// </summary>
    public partial class SpenderManuell_seite : Page
    {

        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);


            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    lbl_betrag.Content = "Betrag";
                    lbl_btnAdd.Content = "Hinzufügen";
                    lbl_btnAdd2.Content = "Hinzufügen";
                    lbl_btnPfad.Content = "Durchsuchen";
                    lbl_checkData.Content = "Überprüfung der zu importierenden Daten";
                    lbl_nachname.Content = "Nachname";
                    lbl_nr.Content = "Nr";
                    lbl_ort.Content = "Ort";
                    lbl_PfadAktualisierung.Content = "Pfad zur Konto-Datei";
                    lbl_plz.Content = "PLZ";
                    lbl_strasse.Content = "Straße";
                    lbl_Title.Content = "Fügen Sie manuell einen Spender hinzu";
                    lbl_Title2.Content = "Automatisches hinzufügen von Daten aus Konto";
                    lbl_vorname.Content = "Vorname";
                    lbl_DBPath.Content = "DB-Pfad";
                    tab_Title1.Header = "Manuell";
                    tab_Title2.Header = "Aus Spendenkonto";
                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    lbl_betrag.Content = "Amount";
                    lbl_btnAdd.Content = "Add";
                    lbl_btnAdd2.Content = "Add";
                    lbl_btnPfad.Content = "Search";
                    lbl_checkData.Content = "Check import Data";
                    lbl_nachname.Content = "Surname";
                    lbl_nr.Content = "Number";
                    lbl_ort.Content = "Place";
                    lbl_PfadAktualisierung.Content = "BankAccount Data Path";
                    lbl_plz.Content = "Post Code";
                    lbl_strasse.Content = "Street";
                    lbl_Title.Content = "Add donor manual";
                    lbl_Title2.Content = "Add donation automaticly from Data";
                    lbl_vorname.Content = "Name";
                    lbl_DBPath.Content = "DB-Path";
                    tab_Title1.Header = "Manual";
                    tab_Title2.Header = "Automaticly";

                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    lbl_betrag.Content = "El importe";
                    lbl_btnAdd.Content = "Agregar";
                    lbl_btnAdd2.Content = "Agregar";
                    lbl_btnPfad.Content = "Buscar";
                    lbl_checkData.Content = "Comprobar datos";
                    lbl_nachname.Content = "Primer nombre";
                    lbl_nr.Content = "Número";
                    lbl_ort.Content = "Lugar";
                    lbl_PfadAktualisierung.Content = "Ruta al archivo bancario";
                    lbl_plz.Content = "Código postal";
                    lbl_strasse.Content = "Calle";
                    lbl_Title.Content = "Añadir una donación manualmente";
                    lbl_Title2.Content = "Añadir una donación automáticamente";
                    lbl_vorname.Content = "primer nombre";
                    lbl_DBPath.Content = "DB-Camino";
                    tab_Title1.Header = "A mano";
                    tab_Title2.Header = "Automáticamente";

                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    lbl_betrag.Content = "Montant";
                    lbl_btnAdd.Content = "Ajouter";
                    lbl_btnAdd2.Content = "Ajouter";
                    lbl_btnPfad.Content = "Chercher";
                    lbl_checkData.Content = "Überprüfung der zu importierenden Daten";
                    lbl_nachname.Content = "nom de famille";
                    lbl_nr.Content = "Nombre";
                    lbl_ort.Content = "Lieu";
                    lbl_PfadAktualisierung.Content = "Chemin vers le fichier bancaire";
                    lbl_plz.Content = "Code postal";
                    lbl_strasse.Content = "Rue";
                    lbl_Title.Content = "Ajouter un don manuel";
                    lbl_Title2.Content = "Ajouter un don automatique";
                    lbl_vorname.Content = "Prénom";
                    lbl_DBPath.Content = "DB-Chemin";
                    tab_Title1.Header = "Manuellement";
                    tab_Title2.Header = "Automatiquement";

                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content
                    lbl_betrag.Content = "сумма";
                    lbl_btnAdd.Content = "добавлять";
                    lbl_btnAdd2.Content = "добавлять";
                    lbl_btnPfad.Content = "поиск";
                    lbl_checkData.Content = "Проверить файл импорта";
                    lbl_nachname.Content = "Фамилия";
                    lbl_nr.Content = "Число";
                    lbl_ort.Content = "Орт";
                    lbl_PfadAktualisierung.Content = "Путь к банковскому файлу";
                    lbl_plz.Content = "Почтовый индекс";
                    lbl_strasse.Content = "улица";
                    lbl_Title.Content = "Добавить донора";
                    lbl_Title2.Content = "Добавить автоматический донор";
                    lbl_vorname.Content = "название";
                    lbl_DBPath.Content = "DB-путь";
                    tab_Title1.Header = "вручную";
                    tab_Title2.Header = "автоматически";

                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }

        public SpenderManuell_seite()
        {
            InitializeComponent();
            SpracheEinstellen();
            //TB_DBPath.Text = Environment.CurrentDirectory + "\\" + Properties.Settings.Default.DB_Name;
            TB_DBPath.Text = Properties.Settings.Default.DB_Path;


        }


        
        //Bei Init der Seite Variablen für den Spender anlegen
        string var_vorname;
        string var_nachname;
        string var_plz;
        string var_ort;
        string var_strasse;
        string var_nummer;
        string var_iban;
        string var_bic;
        string var_betrag;
        string var_waehrung;
        DateTime var_zeitstempel;


        // Hinzufügen Button Start
        //__________________________________________________________________________



        //Switch Icon

        private void Btn_addDonator_Manual_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_KontoFilePath_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));
        }
        private void Btn_addDonator_Manual_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_KontoFilePath_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));
        }

        //Switch Icon

        private void Btn_addDonator_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_addDonater_Auto_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));
        }
        private void Btn_addDonator_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_addDonater_Auto_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));

        }

        //Click Event
        private void Btn_addDonator_Click(object sender, RoutedEventArgs e)
        {
            //Schreibe Textblock Werte in Variablen bei Button klick
            var_vorname = tb_vorname.Text;
            var_nachname = tb_nachname.Text;
            var_plz = tb_plz.Text;
            var_ort = tb_ort.Text;
            var_strasse = tb_strasse.Text;
            var_nummer = tb_nummer.Text;
            var_iban = tb_iban.Text;
            var_bic = tb_bic.Text;
            var_waehrung = lb_waehrung.SelectedItem.ToString();
            var_betrag = tb_betrag.Text;
            int countSpender = 0;

            // Pfad zur DB in Variable schreiben
            string dbPath = Environment.CurrentDirectory + "\\" + Properties.Settings.Default.DB_Name;

            //Datenbankverbindung herstellen
            using (SQLiteConnection con = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;"))
            {
                // Verbindung öffnen
                con.Open();
                try
                {
                    // Neuen Command erstellen
                    SQLiteCommand cmd = new SQLiteCommand(con);

                    // Anzahl vorhandener Namen auf aktuellen Namen ausgeben
                    cmd.CommandText = $"SELECT count(*) FROM Spender WHERE Name= '{var_nachname}';";

                    // Schreibe Anzahl in Variable
                    countSpender = Convert.ToInt32(cmd.ExecuteScalar());

                    if ((countSpender == 0) & (var_betrag != String.Empty) & (var_nachname != String.Empty) & (var_vorname != String.Empty)) // Falls kein Spender mit dem Namen vorhanden & die Mindesteingaben erfüllt sind
                    {
                        // alle Daten in die Tabelle "Spender"
                        cmd.CommandText = $"INSERT INTO Spender (Name, Vorname, PLZ, Ort, Strasse, Hausnr, IBAN, BIC)" +
                            $"VALUES ('{var_nachname}', '{var_vorname}', '{var_plz}', '{var_ort}', '{var_strasse}', '{var_nummer}', '{var_iban}', '{var_bic}')";
                        cmd.ExecuteNonQuery();
                    }
                }

                // Fange Exception, alls ein Fehler auftritt
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message, "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);
                }


            }                                                                                        


                //Checke Felder auf zu wenige Eingabe Werte
                if ((var_betrag != String.Empty) & (var_nachname!=String.Empty) & (var_vorname != String.Empty))    //Schreibe die csv nur, wenn die Mindestangaben ausgefüllt sind
            {
               
                //Benutze Steamwriter um csv zu schreiben
                using (StreamWriter sw = new StreamWriter("../../../Daten/SpenderKartei.csv", true))
                {
                    //Test für schreiben in .csv
                    sw.Write(var_nachname);
                    sw.Write(",");
                    sw.Write(var_vorname);
                    sw.Write(",");
                    sw.Write(var_betrag);
                    sw.Write(",");
                    sw.Write(var_waehrung);
                    sw.Write(",");
                    sw.Write(var_zeitstempel);
                    sw.Write(",");
                    sw.Write(var_iban);
                    sw.Write(",");
                    sw.Write(var_bic);
                    sw.Write(",");
                    sw.Write(var_strasse);
                    sw.Write(",");
                    sw.Write(var_nummer);
                    sw.Write(",");
                    sw.Write(var_plz);
                    sw.Write(",");
                    sw.Write(var_ort);
                    sw.Write("\n");
                }

                MessageBox.Show("Der Spender wurde erfolgreich hinzugefügt.", "Erfolg!", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            else if ((var_betrag == String.Empty) & ((var_nachname == String.Empty) | (var_vorname == String.Empty)))
            {
                MessageBox.Show("Bitte überprüfen Sie die Angaben auf Vollständigkeit.", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);     // Fehlermeldung ausgeben
            }
            else if (var_betrag == String.Empty)
            {
                MessageBox.Show("Bitte geben Sie einen Betrag an.", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);      // Fehlermeldung ausgeben
            }
            else if ((var_nachname==String.Empty) | (var_vorname == String.Empty))
            {
                MessageBox.Show("Bitte geben Sie einen gültigen Namen an.", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Error);     // Fehlermeldung ausgeben
            }

        }


        //Hinzufügen Button für Automatisches Konto Auslesen
        //_______________________________________________________________________________________
        //Click Event

        private void Btn_KontoFilePath_Click(object sender, RoutedEventArgs e)
        {

            //Öffne Auswahlfenster zur Auswahl der Datei
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "CSV Dateien (*.csv)|*.csv";
                ofd.ValidateNames = true;
                ofd.Multiselect = false;

                Nullable<bool> result = ofd.ShowDialog();
                if (result == true)
                {
                    TB_Pfad.Text = System.IO.Path.GetFileName(ofd.FileName); // Füge Dateinamen in Textfeld ein
                    TB_Helper.Text = ofd.FileName;
                    KontoAktualisierung kontoAktualisierung = new KontoAktualisierung(); // Neue Instanz der Klasse KontoAktualisierung erzeugen
                    
                    datagrid.ItemsSource = kontoAktualisierung.ReadCSV(ofd.FileName).DefaultView; // Zeige Daten im Fenster an
                    
                }
            }
            //Gebe Fehler mit seiner zugehoerigen Meldung als PopUp Fenster aus
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        

        //Hover Effekt
        private void Btn_KontoFilePath_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_KontoFilePath_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));

        }
        //Hover Effekt
        private void Btn_KontoFilePath_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_KontoFilePath_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));

        }

        private void Btn_addDonator_Auto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string dbPath = Properties.Settings.Default.DB_Path; // Pfad zur DB in Variable schreiben
                //string dbPath = Environment.CurrentDirectory + "\\" + Properties.Settings.Default.DB_Name; // Pfad zur DB in Variable schreiben
                KontoAktualisierung kontoAktualisierung2 = new KontoAktualisierung(); // Neue Instanz der Klasse KontoAktualisierung erzeugen
                DataTable dataTable = kontoAktualisierung2.ReadCSV(TB_Helper.Text); // Daten aus CSV in dataTable schreiben
                kontoAktualisierung2.WriteCSVtoSQL(dbPath, dataTable); // Funktion WriteCSVtoSQL aufrufen
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fehler", MessageBoxButton.OK, MessageBoxImage.Error); //Fehlermeldung ausgeben
            }
           
        }






        //Hinzufügen Button Ende
        //_____________________________________________________________________________


    }
    
}
