﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Data.SQLite;
using LiveCharts;
using LiveCharts.Wpf;




namespace SpendenverwaltungsSoftware
{
    /// <summary>
    /// Interaktionslogik für Dashboard_Seite.xaml
    /// </summary>
    public partial class Dashboard_Seite : Page
    {


        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);


            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "Aktualiseren";

                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "Refresh";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "actualización";

                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "renouveler";

                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "актуализировать";

                    break;
            }
            //_________________________________________________________


            return SprachEinstellung;





        }

        public string Anfrage(string SqlKommando)
        {
            //Erstelle Variable für relativen Datenbankpfad
            string result = "";
            string ConnectionStringDB = Environment.CurrentDirectory + "\\SpenderVerzeichnis.db;" + "Version=3;";
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + ConnectionStringDB);
            try
            {
                sqlite.Open();  //Verbindung aufbauen
                SQLiteCommand cmd = sqlite.CreateCommand();
                cmd.CommandText = SqlKommando;  //Anfrage an Datenbank
                result = cmd.ExecuteScalar().ToString();
                Console.WriteLine(result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //Verbindung schließen
                sqlite.Close();
            }
            return result;
        }


        public void DashboardWerteBerechnen()
        {

            //Variablen für Dashboard
            string MaxSpenderAnzahl = "";
            string SpendenGeldMenge = "";
            string letzteSpende = "";

            //Maximale SpenderAnzahl ausgeben
            MaxSpenderAnzahl = Anfrage("SELECT max(SpenderID) FROM Spender;");
            lbl_SpenderAnzahl.Content = MaxSpenderAnzahl;

            //GesamteSpenden Summe bilden
            SpendenGeldMenge = Anfrage("SELECT SUM(Betrag) FROM Spende;");
            SpendenGeldMenge = SpendenGeldMenge + " Euro";
            lbl_SpendenBetragSumme.Content = SpendenGeldMenge;

            //Letzte Spende ermitteln
            letzteSpende = Anfrage("SELECT max(Datum) FROM Spende;");
            SpendenGeldMenge = letzteSpende;
            lbl_letzteSpendeDatum.Content = letzteSpende;           

        }

        //Fuer Unit Test
        //_____________________________________________________________________________
        public bool DashboardWerteBerechnenTEST(string MaxSpenderAnzahlTEST,string SpendenGeldMengeTEST, string letzteSpendeTEST)
        {

            //Variablen für Dashboard
            string MaxSpenderAnzahl = MaxSpenderAnzahlTEST;
            string SpendenGeldMenge = SpendenGeldMengeTEST;
            string letzteSpende = letzteSpendeTEST;

            //Maximale SpenderAnzahl ausgeben
            lbl_SpenderAnzahl.Content = MaxSpenderAnzahlTEST;

            //GesamteSpenden Summe bilden
            lbl_SpendenBetragSumme.Content = SpendenGeldMenge;

            //Letzte Spende ermitteln
            SpendenGeldMenge = letzteSpendeTEST;
            lbl_letzteSpendeDatum.Content = letzteSpende;

            // Prüfe ob Labels den Inhalt besitzen und gebe Ergebnis an Unit Test zurück
            if(lbl_SpenderAnzahl.Content == MaxSpenderAnzahlTEST && 
               lbl_SpendenBetragSumme.Content == SpendenGeldMengeTEST &&
               lbl_letzteSpendeDatum.Content == letzteSpendeTEST)
            {

                return true;

            }
            {
                return false;
            }

        }


        public Dashboard_Seite()
        {

            InitializeComponent();
            SpracheEinstellen();
            ChartAnzahlPlot();
            DashboardWerteBerechnen();

        }


        //Funktion Werte für Chart Anzahl
        //__________________________________________________________________________________________
        public SeriesCollection ChartAnzahlData { get; private set; }

        public void ChartAnzahlPlot()
        {

            //Neue Series Collection für Chart Daten erstellen
            ChartAnzahlData = new SeriesCollection();

            //Erstelle mehrere LineSeries (Werte)
            var Testwert11Series = new LineSeries
            {
                Title = "Testwert11",
                Values = new ChartValues<double> { 10, 5, 7, 5, 7, 8 }
            };

            var Testwert12Series = new LineSeries
            {
                Title = "Testwert12",
                Values = new ChartValues<double> { 5, 6, 9, 10, 11, 9 }
            };

            //add our series to our SeriesCollection
            ChartAnzahlData.Add(Testwert11Series);
            ChartAnzahlData.Add(Testwert12Series);

            //that's it, LiveCharts is ready and listening for your data changes.
            DataContext = this;
        }
        //Ende Chart Anzahl_________________________________________________________________________
        //__________________________________________________________________________________________







        private void Btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            DashboardWerteBerechnen();
            ChartAnzahlPlot();
        }

        private void Btn_refresh_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_refresh_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));

        }

        private void Btn_refresh_MouseLeave(object sender, MouseEventArgs e)
        {

            btn_refresh_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));

        }





    }
}
