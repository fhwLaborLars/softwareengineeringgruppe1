﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Data;
using System.Data.OleDb;
using Microsoft.Win32;
using Path = System.IO.Path;
using System.Data.SQLite;


namespace SpendenverwaltungsSoftware
{
    /// <summary>
    /// Interaktionslogik für SpenderRegister_seite.xaml
    /// </summary>
    public partial class SpenderRegister_seite : Page
    {
        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);


            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "Aktualisieren";
                    lbl_Title.Content = "Anzeige der Spenderkartei";

                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "Refresh";
                    lbl_Title.Content = "Display donor database";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "Actualización";
                    lbl_Title.Content = "Visualización Base de datos de donantes";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    lbl_btnRefresh.Content = "Renouveler";
                    lbl_Title.Content = "Afficher Base de données des donateurs";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    lbl_btnRefresh.Content = "обновление";
                    lbl_Title.Content = "дисплей База данных доноров";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }

        // Funktion zum Aktualisieren der Daten Information
        private void DatenAktualisieren()
        {


            //Erstelle Variable für relativen Datenbankpfad
            string ConnectionStringDB = Environment.CurrentDirectory + "\\SpenderVerzeichnis.db;" + "Version=3;";
            Console.WriteLine("Relativer Datenbankpfad =  " + ConnectionStringDB);
            //Stelle Verbindung zur Sqlite DB her
            SQLiteConnection SpenderVerzeichnis_DB_conn;
            SpenderVerzeichnis_DB_conn = new SQLiteConnection("Data Source=" + ConnectionStringDB);
            SpenderVerzeichnis_DB_conn.Open();

            //Frage Werte aus DB ab
            string sql = "select SpenderID,Name,Vorname,PLZ,Ort,Strasse,Hausnr,IBAN,BIC from Spender";
            SQLiteCommand consolecommand = new SQLiteCommand(sql, SpenderVerzeichnis_DB_conn);

            //Schreibe Werte ins Log zur Prüfung //!! WORKING
            SQLiteDataReader reader = consolecommand.ExecuteReader();
            while (reader.Read())
                Console.WriteLine("SpenderID: " + reader["SpenderID"] + "\rVorname: " + reader["Vorname"] + "\rPLZ: " + reader["PLZ"] + "\rOrt: " + reader["Ort"] + "\rStrasse: " + reader["Strasse"] + "\rHausnummer: " + reader["Hausnr"] + "\rIBAN: " + reader["IBAN"] + "\rBIC: " + reader["BIC"]);

            

            //Schreibe Sqlite Werte in DataTable

            try
            {

                using (SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(sql, SpenderVerzeichnis_DB_conn))
                {
                    DataTable dataTable = new DataTable();
                    dataAdapter.Fill(dataTable);
                    SpenderVerzeichnisGrid.ItemsSource = dataTable.AsDataView();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //Schließe Verbindung
            SpenderVerzeichnis_DB_conn.Close();
            SpenderVerzeichnis_DB_conn.Dispose();

        }



        public SpenderRegister_seite()
        {
            InitializeComponent();
            SpracheEinstellen();
            //Erstelle Variable für relativen Datenbankpfad
            string ConnectionStringDB = "@" + '\u0022' + "Data Source=" + Environment.CurrentDirectory + "\\SpenderVerzeichnis.db;" + "Version=3;" + '\u0022';
            Console.WriteLine("Relativer Datenbankpfad =  " + ConnectionStringDB);
            //Beim Öffnen der Seite direkt Datentable aktualisieren
            DatenAktualisieren();
        }



        //Refresh Button Aktionen
        //_______________________________________________________________________________________
        //Click Event
        private void Btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            //Manuelles Aktualisieren der Daten
            DatenAktualisieren();
        }








        //Icon Change
        private void Btn_refresh_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_refresh_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));
        }
        //Icon Change
        private void Btn_refresh_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_refresh_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));
        }


    }


//Close Button Aktionen
//______________________________________________________________________________________
}
