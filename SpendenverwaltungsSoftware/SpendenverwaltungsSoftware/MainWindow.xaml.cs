﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.IO;

namespace SpendenverwaltungsSoftware
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //Initialisiere Variablen für Fenster Steuerung -> Weise den Fenstern.xaml Namen zu mit denen Sie kontrolliert werden können
        //Die Festgelegten Namen bei den Fenster Kontroll Buttons benutzen :
        //_____________________________________________________________________________
        /*
        ManuelleEingabeSpender ManualInputWindow = new ManuelleEingabeSpender();
        Einstellungen SettingsWindow = new Einstellungen();
        Spendenbescheinigung CertificateWindow = new Spendenbescheinigung();
        SpenderRegister RegistryWindow = new SpenderRegister();
        Statistiken StatisticsWindow = new Statistiken();
        Dashboard DashboardWindow = new Dashboard();
        //Weitere Fenster Hier bennenen
        */
        //_____________________________________________________________________________
        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);


            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    lbl_Bescheinigung.Content = "Bescheinigung";
                    lbl_btnDashboard.Content = "Übersicht";
                    lbl_btnRegistry.Content = "Register";
                    lbl_btnManual.Content = "Hinzufügen";
                    lbl_btnSettings.Content = "Einstellungen";
                    lbl_btnStatistics.Content = "Statistiken";

                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    lbl_Bescheinigung.Content = "Certificat";
                    lbl_btnDashboard.Content = "Dashboard";
                    lbl_btnRegistry.Content = "Registry";
                    lbl_btnManual.Content = "Add manual";
                    lbl_btnSettings.Content = "Settings";
                    lbl_btnStatistics.Content = "Statistics";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    lbl_Bescheinigung.Content = "Certificado";
                    lbl_btnDashboard.Content = "Tablero";
                    lbl_btnRegistry.Content = "Registro";
                    lbl_btnManual.Content = "Añadir";
                    lbl_btnSettings.Content = "Ajustes";
                    lbl_btnStatistics.Content = "Estadística";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    lbl_Bescheinigung.Content = "Certificat";
                    lbl_btnDashboard.Content = "Enquête";
                    lbl_btnRegistry.Content = "S'inscrire";
                    lbl_btnManual.Content = "Ajouter";
                    lbl_btnSettings.Content = "Paramètres";
                    lbl_btnStatistics.Content = "Statistiques";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content 
                    lbl_Bescheinigung.Content = "сертификат";
                    lbl_btnDashboard.Content = "обозрение";
                    lbl_btnRegistry.Content = "регистр";
                    lbl_btnManual.Content = "добавлять";
                    lbl_btnSettings.Content = "настройки";
                    lbl_btnStatistics.Content = "статистика";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }

        public MainWindow()
        {
            //Beim Initialisieren des MainWindow im Content Bereich das Dashboard aufrufen
            // HauptContentFrame.Content = new Dashboard_Seite();
            InitializeComponent();
            SpracheEinstellen();
            HauptContentFrame.Content = new Dashboard_Seite();


        }


        //Navigationslogik Bereich für die Benutzeroberfläche
        //_______________________________________________________________________________

        //´Settings Button Aktionen
        //_____________________________________________________________________________________
        //Click Event
        private void Btn_settings_Click(object sender, RoutedEventArgs e)
        {

            HauptContentFrame.Content = new Einstellungen_Seite();

        }
        //Change Icon
        private void Btn_settings_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_Settings_IMG.Source = new BitmapImage(new Uri("/Resources/settingslMouseover_icon.png", UriKind.RelativeOrAbsolute));
        }
        //Change Icon
        private void Btn_settings_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_Settings_IMG.Source = new BitmapImage(new Uri("/Resources/settings_icon.png", UriKind.RelativeOrAbsolute));

        }
        //END Button Settings_______________________________________________


        //Certificate Button Aktionen
        //_____________________________________________________________________________________
        //Click Event
        private void Btn_certificate_Click(object sender, RoutedEventArgs e)
        {

            HauptContentFrame.Content = new Bescheinigung_seite();
        }
        //Change Icon
        private void Btn_certificate_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_paper_IMG.Source = new BitmapImage(new Uri("/Resources/paperlMouseover_icon.png", UriKind.RelativeOrAbsolute));
        }
        //Change Icon
        private void Btn_certificate_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_paper_IMG.Source = new BitmapImage(new Uri("/Resources/paper_icon.png", UriKind.RelativeOrAbsolute));

        }
        //END Button Certificate______________________________________________


        //Statisctics Button Aktionen
        //_____________________________________________________________________________________
        //Click Event
        private void Btn_statistics_Click(object sender, RoutedEventArgs e)
        {
            HauptContentFrame.Content = new Statistiken_Seite();

        }
        private void Btn_statistics_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_statistics_IMG.Source = new BitmapImage(new Uri("/Resources/statisticslMouseover_icon.png", UriKind.RelativeOrAbsolute));

        }

        private void Btn_statistics_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_statistics_IMG.Source = new BitmapImage(new Uri("/Resources/statistics_icon.png", UriKind.RelativeOrAbsolute));
        }
        //END Button Statistics______________________________________________



        //ManualInput Button Aktionen
        //_____________________________________________________________________________________
        //Click Event       
        private void Btn_manualinputwindow_Click(object sender, RoutedEventArgs e)
        {
            HauptContentFrame.Content = new SpenderManuell_seite();


        }
        private void Btn_manualinputwindow_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_manual_IMG.Source = new BitmapImage(new Uri("/Resources/paperlMouseover_icon.png", UriKind.RelativeOrAbsolute));
        }

        private void Btn_manualinputwindow_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_manual_IMG.Source = new BitmapImage(new Uri("/Resources/paper_icon.png", UriKind.RelativeOrAbsolute));
        }
        //End Registry Button___________________________________________________________________



        //Registry Button Aktionen
        //_____________________________________________________________________________________
        //Click Event
        private void Btn_registry_Click(object sender, RoutedEventArgs e)
        {
            HauptContentFrame.Content = new SpenderRegister_seite();

        }
        private void Btn_registry_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_registry_IMG.Source = new BitmapImage(new Uri("/Resources/registrylMouseover_icon.png", UriKind.RelativeOrAbsolute));
        }

        private void Btn_registry_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_registry_IMG.Source = new BitmapImage(new Uri("/Resources/registry_icon.png", UriKind.RelativeOrAbsolute));
        }

        //End Registry Button___________________________________________________________________



        //Dashboard Button Aktionen
        //_____________________________________________________________________________________
        //Click Event
        private void Btn_dashboard_Click(object sender, RoutedEventArgs e)
        {

            HauptContentFrame.Content = new Dashboard_Seite();

        }
        //Change Icon
        private void Btn_dashboard_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_Dashboard_IMG.Source = new BitmapImage(new Uri("/Resources/dashboardlMouseover_icon.png", UriKind.RelativeOrAbsolute));

        }
        //Change Icon
        private void Btn_dashboard_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_Dashboard_IMG.Source = new BitmapImage(new Uri("/Resources/dashboard_icon.png", UriKind.RelativeOrAbsolute));

        }
        //End Dashboard Button___________________________________________________________________




        //Close Button Aktionen
        //________________________________________________________________________________________
        //Click Event
        private void Btn_closeprog_Click(object sender, RoutedEventArgs e)
        {
            //Schließe Programm beim Klicken
            Application.Current.Shutdown();

        }
        //Change Icon
        private void Btn_closeprog_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_close_IMG.Source = new BitmapImage(new Uri("/Resources/closeMouseover_icon.png", UriKind.RelativeOrAbsolute));
        }
        //Change Icon

        private void Btn_closeprog_MouseLeave(object sender, MouseEventArgs e)
        {
             btn_close_IMG.Source = new BitmapImage(new Uri("/Resources/close_icon.png", UriKind.RelativeOrAbsolute));

        }



        //END Close Programm Button


        //Navigationsbereich Ende
        //_______________________________________________________________________________






        //Menu Leiste____________________________________________________________________


        //MenuLeiste Drag-Window

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }













        //MenuLeiste Ende





        //Menu Leiste Ende____________________________________________________________________
















    }
}
