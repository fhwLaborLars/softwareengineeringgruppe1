﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;


namespace SpendenverwaltungsSoftware
{
    
    /// <summary>
    /// Interaktionslogik für Einstellungen_Seite.xaml
    /// </summary>
    public partial class Einstellungen_Seite : Page
    {


        public string SpracheEinstellen()
        {

            string SprachEinstellung;
            var FileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");
            Console.WriteLine("Pfad der Settings.ini:  " + AppDomain.CurrentDomain.BaseDirectory + "Settings.ini");


            SprachEinstellung = FileStream.ReadLine();
            //string[] Ergebnis = SprachEinstellung.Split(new string[] {"="}, StringSplitOptions.None);
            Console.WriteLine("Eingestellte Sprache:" + SprachEinstellung);


            //Suche Spracheinstellung und setzt Content auf Sprache
            //______________________________________________________

            switch (SprachEinstellung)
            {
                case "Sprache=Deutsch":
                    Console.WriteLine("Deutsch");
                    SprachEinstellung = "Deutsch";
                    //Hier Lang. Content
                    lbl_btnAdd.Content = "Hinzufügen";
                    lbl_btnSave.Content = "Übernehmen";
                    lbl_btnSearch.Content = "Durchsuchen";
                    lbl_DBPath.Content = "Pfad zur Datenbank";
                    lbl_Sprache.Content = "Sprache";
                    lbl_Title.Content = "Programmeinstellungen";
                    break;
                case "Sprache=Englisch":
                    Console.WriteLine("Englisch");
                    SprachEinstellung = "Englisch";
                    //Hier Lang. Content
                    lbl_btnAdd.Content = "Add";
                    lbl_btnSave.Content = "Save";
                    lbl_btnSearch.Content = "Search";
                    lbl_DBPath.Content = "Database Path";
                    lbl_Sprache.Content = "Language";
                    lbl_Title.Content = "Program Settings";
                    break;
                case "Sprache=Spanisch":
                    Console.WriteLine("Spanisch");
                    SprachEinstellung = "Spanisch";
                    //Hier Lang. Content
                    lbl_btnAdd.Content = "Añadir";
                    lbl_btnSave.Content = "Salvar";
                    lbl_btnSearch.Content = "Buscar";
                    lbl_DBPath.Content = "Camino Base de datos";
                    lbl_Sprache.Content = "Idioma";
                    lbl_Title.Content = "Programa Ajustes";
                    break;
                case "Sprache=Französisch":
                    Console.WriteLine("Französisch");
                    SprachEinstellung = "Französisch";
                    //Hier Lang. Content
                    lbl_btnAdd.Content = "Ajouter";
                    lbl_btnSave.Content = "Appliquer";
                    lbl_btnSearch.Content = "chercher";
                    lbl_DBPath.Content = "Chemin Base de données";
                    lbl_Sprache.Content = "Les langue";
                    lbl_Title.Content = "Programme réglages";
                    break;
                case "Sprache=Russisch":
                    Console.WriteLine("Russisch");
                    SprachEinstellung = "Russisch";
                    //Hier Lang. Content
                    lbl_btnAdd.Content = "добавлять";
                    lbl_btnSave.Content = "применять";
                    lbl_btnSearch.Content = "поиск";
                    lbl_DBPath.Content = "путь база данных";
                    lbl_Sprache.Content = "язык";
                    lbl_Title.Content = "программа настройки";
                    break;
            }
            //_________________________________________________________
            this.UpdateLayout();
            return SprachEinstellung;
        }


        public Einstellungen_Seite()
        {

            InitializeComponent();
            UpdateLayout();
            SpracheEinstellen();
            tb_dbdir.Text = Properties.Settings.Default.DB_Name;

        }



        public void SpracheEinstellung()
        {
            //Initialisiere Variablen
            string Sprachauswahl;
            Sprachauswahl = "NoItem";

            //Weise den Flaggen aus dem Container mit ID entsprechende Strings zu
            //_________________________________________________________
            switch (lb_Sprache.SelectedIndex)
            {
                case -1:
                    Console.WriteLine("Kein Item ");
                    Sprachauswahl = "noitem";
                    break;
                case 0:
                    Console.WriteLine("Deutsch");
                    Sprachauswahl = "Deutsch";
                    break;
                case 1:
                    Console.WriteLine("Englisch");
                    Sprachauswahl = "Englisch";
                    break;
                case 2:
                    Console.WriteLine("Spanisch ");
                    Sprachauswahl = "Spanisch";
                    break;
                case 3:
                    Console.WriteLine("Französisch");
                    Sprachauswahl = "Französisch";
                    break;
                case 4:
                    Console.WriteLine("Russisch");
                    Sprachauswahl = "Russisch";
                    break;
            }

            //Schreibe die Ausgewählte Sprache in die Settings.ini
            //_________________________________________________________
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "\\Settings.ini", "Sprache=" + Sprachauswahl);
        }   



        // EinstellungenSpeichern Button Start
        //__________________________________________________________________________

        //Change Icon

        private void Btn_saveSettings_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_SaveSettings_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));

        }
        //Change Icon

        private void Btn_saveSettings_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_SaveSettings_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));

        }


        //Click Event
        private void Btn_saveSettings_Click(object sender, RoutedEventArgs e)
        {


            //Frage ab, ob der Nutzer sicher ist die Einstellungen zu ändern.
            MessageBoxResult result = MessageBox.Show("Sind Sie sich sicher, dass die Einstellungen gespeichert werden sollen?", "Apply Settings", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    SpracheEinstellung();
                    //Sprache Einstellen im Menü     
                    SpracheEinstellen();
                    //DatenbankPfad Speichern
                    Properties.Settings.Default.DB_Name = tb_dbdir.Text;
                    Properties.Settings.Default.DB_Path = TB_helper.Text;
                    Properties.Settings.Default.Save();
                    //Starte Applikation neu, um alle Seiten zu aktualisieren.
                    MessageBox.Show("Das Programm wird neugestartet, um die Änderungen zu übernehmen", "Neustart", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.No);
                    Application.Current.Shutdown();
                    System.Diagnostics.Process.Start(Environment.GetCommandLineArgs()[0]);
                    break;
                case MessageBoxResult.No:
                    break;

            }







        }

        private void Btn_DBPath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "SQLite Datenbank (*.db)|*.db";
                ofd.ValidateNames = true;
                ofd.Multiselect = false;
                
                Nullable<bool> result = ofd.ShowDialog();
                if (result == true)
                {

                    tb_dbdir.Text = System.IO.Path.GetFileName(ofd.FileName); // Füge Dateinamen in Textfeld ein
                    TB_helper.Text = ofd.FileName;

                }
            }

            //Gebe Fehler mit seiner zugehoerigen Meldung als PopUp Fenster aus
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Btn_DBPath_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_DBPath_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));
        }

        private void Btn_DBPath_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_DBPath_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));
        }

        private void Btn_finanzamt_MouseEnter(object sender, MouseEventArgs e)
        {
            btn_Finanzamz_IMG.Source = new BitmapImage(new Uri("/Resources/btnHover_icon.png", UriKind.RelativeOrAbsolute));

        }

        private void Btn_finanzamt_MouseLeave(object sender, MouseEventArgs e)
        {
            btn_Finanzamz_IMG.Source = new BitmapImage(new Uri("/Resources/btnNormal_icon.png", UriKind.RelativeOrAbsolute));

        }
        // EinstellungenSpeichern Button Start
        //__________________________________________________________________________


    }
}
