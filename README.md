![](SpendenverwaltungsSoftware/Resourcen/FHW.png)
# Spendenverwaltungs Software
### Eine Software zur Erfassung von Spendengeldern und Spenden
Diese Software wurde im Rahmen des Moduls Software Engineering an der Fachhochschule Westküste im 6. Semester entwickelt.


---

_**Aktuelle**_ kompilierte Version der Software ist immer unter folgendem **Link** verfügbar:

[Download der aktuellen Version des Programms Stand vom 10.06.2019](https://bitbucket.org/fhwLaborLars/softwareengineeringgruppe1/downloads/SpendenVerwaltung.iso)


---
![](SpendenverwaltungsSoftware/Resourcen/Cover.png)

---
###Features der Software
* Datenbank Architektur auf Basis von SQLite
* Intuitives Design
* Automatisches Auswerten von Kontoinformationen
* Manuelles Editieren der Datenbankeinträge
* Spracheinstellungen für Deutsch,Englisch,Russisch,Spanisch und Französisch
---

#####Copyright 2019 |  HeLaTi Software Team

######Timo Kähding
######Heiko Waurisch
######Lars Gaumer